﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Blazor.Extensions;
using Blazor.Extensions.Canvas.WebGL;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

#pragma warning disable 649

namespace BlazorWASMTest.Shared
{
    public interface IBlazorRenderer : IDisposable
    {
        ValueTask SetContext(WebGLContext ctx);

        ValueTask Render(float dt);
    }

    public partial class RenderingComponent : IDisposable
    {
        private BECanvasComponent _canvasReference;
        private WebGLContext _context;
        private float _fpsAccumulator;
        private int _framesCounter;
        private long _lastFrameStart;
        private DotNetObjectReference<RenderingComponent> _thisRef;
        public float FPS { get; private set; }

        [Inject] private IJSRuntime _JSRuntime { get; set; }


        [Parameter] public IBlazorRenderer Renderer { get; set; }

        public void Dispose()
        {
            _thisRef?.Dispose();
            _context?.Dispose();
            Renderer?.Dispose();
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!firstRender) return;
            _lastFrameStart = Stopwatch.GetTimestamp();
            _context = await _canvasReference.CreateWebGLAsync(new WebGLContextAttributes {PowerPreference = WebGLContextAttributes.POWER_PREFERENCE_HIGH_PERFORMANCE});
            await Renderer.SetContext(_context);
            _thisRef = DotNetObjectReference.Create(this);
            await _JSRuntime.InvokeVoidAsync("registerGameLoop", _thisRef, nameof(Render));
        }

        [JSInvokable]
        public async ValueTask Render()
        {
            var frameStart = Stopwatch.GetTimestamp();
            var dt = (frameStart - _lastFrameStart) / (float) Stopwatch.Frequency;
            _lastFrameStart = frameStart;

            var renderer = Renderer;
            if (renderer != null) await renderer.Render(dt);
            UpdateFPSDisplay(dt);
        }

        private void UpdateFPSDisplay(float dt)
        {
            _framesCounter++;
            _fpsAccumulator += dt;
            if (_fpsAccumulator > 1f)
            {
                FPS = _framesCounter;
                _fpsAccumulator = 0;
                _framesCounter = 0;
                StateHasChanged();
            }
        }
    }
}