﻿using GlmNet;

namespace BlazorWASMTest.Code
{
    public static class Meshloader
    {
        public static readonly Mesh Cube;

        static Meshloader()
        {
            {
                var indices = new ushort[]
                {
                    0, 1, 2, 0, 2, 3, // front
                    4, 5, 6, 4, 6, 7, // back
                    8, 9, 10, 8, 10, 11, // top
                    12, 13, 14, 12, 14, 15, // bottom
                    16, 17, 18, 16, 18, 19, // right
                    20, 21, 22, 20, 22, 23 // left};
                };
                var positions = new[]
                {
                    // Front face
                    -1.0f, -1.0f, 1.0f,
                    1.0f, -1.0f, 1.0f,
                    1.0f, 1.0f, 1.0f,
                    -1.0f, 1.0f, 1.0f,

                    // Back face
                    -1.0f, -1.0f, -1.0f,
                    -1.0f, 1.0f, -1.0f,
                    1.0f, 1.0f, -1.0f,
                    1.0f, -1.0f, -1.0f,

                    // Top face
                    -1.0f, 1.0f, -1.0f,
                    -1.0f, 1.0f, 1.0f,
                    1.0f, 1.0f, 1.0f,
                    1.0f, 1.0f, -1.0f,

                    // Bottom face
                    -1.0f, -1.0f, -1.0f,
                    1.0f, -1.0f, -1.0f,
                    1.0f, -1.0f, 1.0f,
                    -1.0f, -1.0f, 1.0f,

                    // Right face
                    1.0f, -1.0f, -1.0f,
                    1.0f, 1.0f, -1.0f,
                    1.0f, 1.0f, 1.0f,
                    1.0f, -1.0f, 1.0f,

                    // Left face
                    -1.0f, -1.0f, -1.0f,
                    -1.0f, -1.0f, 1.0f,
                    -1.0f, 1.0f, 1.0f,
                    -1.0f, 1.0f, -1.0f
                };

                var normals = new[]
                {
                    0.0f, 0.0f, 1.0f,
                    0.0f, 0.0f, 1.0f,
                    0.0f, 0.0f, 1.0f,
                    0.0f, 0.0f, 1.0f,

                    0.0f, 0.0f, -1.0f,
                    0.0f, 0.0f, -1.0f,
                    0.0f, 0.0f, -1.0f,
                    0.0f, 0.0f, -1.0f,

                    0.0f, 1.0f, 0.0f,
                    0.0f, 1.0f, 0.0f,
                    0.0f, 1.0f, 0.0f,
                    0.0f, 1.0f, 0.0f,

                    0.0f, -1.0f, 0.0f,
                    0.0f, -1.0f, 0.0f,
                    0.0f, -1.0f, 0.0f,
                    0.0f, -1.0f, 0.0f,

                    1.0f, 0.0f, 0.0f,
                    1.0f, 0.0f, 0.0f,
                    1.0f, 0.0f, 0.0f,
                    1.0f, 0.0f, 0.0f,

                    -1.0f, 0.0f, 0.0f,
                    -1.0f, 0.0f, 0.0f,
                    -1.0f, 0.0f, 0.0f,
                    -1.0f, 0.0f, 0.0f
                };
                var vertices = new VertexData[positions.Length / 3];
                for (var i = 0; i < positions.Length; i += 3)
                {
                    vertices[i / 3].Normal = new vec3(normals[i], normals[i + 1], normals[i + 2]);
                    vertices[i / 3].Position = new vec3(positions[i], positions[i + 1], positions[i + 2]);
                }

                Cube = new Mesh(indices, vertices);
            }
        }
    }
}