﻿using System;
using System.Threading.Tasks;
using Blazor.Extensions.Canvas.WebGL;

namespace BlazorWASMTest.Code
{
    public static class ShaderHelper
    {
        private const string VS_SOURCE = @"
            uniform mat4 WorldViewProjection;
            uniform mat4 WorldView;

            attribute vec4 Pos;
            attribute vec4 Normal;
            varying vec3 TransformedNormal;

            void main() {
                gl_Position = WorldViewProjection * vec4(vec3(Pos), 1.0);
                TransformedNormal = vec3(WorldView * vec4(vec3(Normal), 0.0));
            }";

        private const string FS_SOURCE = @"
            precision mediump float;
            
            uniform vec3 InvLightDirection;

            varying vec3 TransformedNormal;

            void main() {
                float intensity = dot(InvLightDirection, normalize(TransformedNormal));
                intensity = clamp(intensity, 0.0, 0.1);
                gl_FragColor = vec4(vec3(intensity), 1.0);
            }";

        public static ValueTask<WebGLProgram> Create(WebGLContext ctx)
        {
            return CreateProgramAsync(ctx, VS_SOURCE, FS_SOURCE);
        }

        private static async ValueTask<WebGLProgram> CreateProgramAsync(WebGLContext gl, string vsSource, string fsSource)
        {
            var vertexShader = await CreateShaderAsync(gl, ShaderType.VERTEX_SHADER, vsSource);
            var fragmentShader = await CreateShaderAsync(gl, ShaderType.FRAGMENT_SHADER, fsSource);

            var program = await gl.CreateProgramAsync();
            await gl.AttachShaderAsync(program, vertexShader);
            await gl.AttachShaderAsync(program, fragmentShader);
            await gl.LinkProgramAsync(program);

            await gl.DeleteShaderAsync(vertexShader);
            await gl.DeleteShaderAsync(fragmentShader);

            if (!await gl.GetProgramParameterAsync<bool>(program, ProgramParameter.LINK_STATUS))
            {
                var info = await gl.GetProgramInfoLogAsync(program);
                throw new Exception("An error occured while linking the program: " + info);
            }

            return program;
        }

        private static async ValueTask<WebGLShader> CreateShaderAsync(WebGLContext gl, ShaderType type, string source)
        {
            var shader = await gl.CreateShaderAsync(type);

            await gl.ShaderSourceAsync(shader, source);
            await gl.CompileShaderAsync(shader);

            if (!await gl.GetShaderParameterAsync<bool>(shader, ShaderParameter.COMPILE_STATUS))
            {
                var info = await gl.GetShaderInfoLogAsync(shader);
                await gl.DeleteShaderAsync(shader);
                throw new Exception("An error occured while compiling the shader: " + info);
            }

            return shader;
        }
    }
}