﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Blazor.Extensions.Canvas.WebGL;
using BlazorWASMTest.Shared;
using GlmNet;

namespace BlazorWASMTest.Code
{
    public class Game : IBlazorRenderer
    {
        private readonly vec3 _cameraPos = new vec3(0, -15, 8);
        private readonly List<GameObject> _gameObjects = new List<GameObject>();
        private readonly vec3 _invLightDirection = glm.normalize(new vec3(1, 1, 1));
        private readonly vec3 _cameraTarget = default;


        private WebGLContext _ctx;
        private WebGLUniformLocation _invLightDirectionLocation;
        private uint _normalAttribLocation;
        private uint _positionAttribLocation;
        private WebGLProgram _program;
        private WebGLUniformLocation _worldViewLocation;
        private WebGLUniformLocation _worldViewProjectionLocation;
        private float accu;


        public async ValueTask Render(float dt)
        {
            accu += dt;
            {
                //clear
                var bgcolorFade = 1 - 0.8f * CreateColor(accu);
                await _ctx.ClearColorAsync(bgcolorFade, bgcolorFade, 1, 1);
            }
            //frame invariants
            await _ctx.UseProgramAsync(_program);
            await AssertContext();

            var view = glm.lookAt(_cameraPos, _cameraTarget, new vec3(0, 0, 1));
            var projection = glm.perspective(MathF.PI / 2, 16f / 9, 0.001f, 1000);
            var viewProjection = projection * view;
            await _ctx.UniformAsync(_invLightDirectionLocation, new vec3(view * new vec4(_invLightDirection, 0)).to_array());
            await AssertContext();

            {
                //iterate objects
                Mesh lastMesh = null;
                _gameObjects.Sort((a, b) => (a.Renderable.Mesh?.GetHashCode() ?? -1) - (b.Renderable.Mesh?.GetHashCode() ?? -1));
                await _ctx.BeginBatchAsync();
                await _ctx.ClearAsync(BufferBits.COLOR_BUFFER_BIT | BufferBits.DEPTH_BUFFER_BIT);
                await AssertContext();
                await _ctx.EndBatchAsync();
                foreach (var gameObject in _gameObjects)
                {
                    var mesh = gameObject.Renderable.Mesh;
                    if (mesh == null)
                        continue;

                    var world = gameObject.Transform.GetMatrix();
                    await _ctx.UniformMatrixAsync(_worldViewProjectionLocation, false, (viewProjection * world).to_array());
                    await AssertContext();
                    await _ctx.UniformMatrixAsync(_worldViewLocation, false, (view * world).to_array());
                    await AssertContext();

                    if (mesh != lastMesh)
                    {
                        if (lastMesh != null)
                        {
                            await _ctx.EndBatchAsync();
                            await AssertContext();
                        }

                        lastMesh = mesh;
                        await _ctx.BeginBatchAsync();
                        await mesh.BindMesh(_ctx);
                        await AssertContext();
                        await _ctx.EnableVertexAttribArrayAsync(_positionAttribLocation);
                        await AssertContext();
                        await _ctx.EnableVertexAttribArrayAsync(_normalAttribLocation);
                        await AssertContext();
                        await _ctx.VertexAttribPointerAsync(_positionAttribLocation, 4, DataType.FLOAT, false, 8 * sizeof(float), 0);
                        await AssertContext();
                        await _ctx.VertexAttribPointerAsync(_normalAttribLocation, 4, DataType.FLOAT, false, 8 * sizeof(float), 4 * sizeof(float));
                        await AssertContext();
                    }

                    await _ctx.DrawElementsAsync(mesh.Primitive, mesh.IndexCount, DataType.UNSIGNED_SHORT, 0);
                    await AssertContext();
                }

                if (lastMesh != null)
                {
                    await _ctx.EndBatchAsync();
                    await AssertContext();
                }
            }
        }

        public async ValueTask SetContext(WebGLContext ctx)
        {
            if (_ctx != null)
                throw new Exception("Cant init me twice yet.");

            _ctx = ctx;
            await Init();
        }

        public void Dispose()
        {
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private async ValueTask AssertContext([CallerMemberName] string caller = null, [CallerLineNumber] int line = -1)
        {
#if DEBUG
            var error = await _ctx.GetErrorAsync();
            if (error != Error.NO_ERROR)
            {
            }
#endif
        }

        private async ValueTask Init()
        {
            _program = await ShaderHelper.Create(_ctx);
            _worldViewProjectionLocation = await _ctx.GetUniformLocationAsync(_program, "WorldViewProjection");
            _worldViewLocation = await _ctx.GetUniformLocationAsync(_program, "WorldView");
            _invLightDirectionLocation = await _ctx.GetUniformLocationAsync(_program, "InvLightDirection");
            _positionAttribLocation = (uint) await _ctx.GetAttribLocationAsync(_program, "Pos");
            _normalAttribLocation = (uint) await _ctx.GetAttribLocationAsync(_program, "Normal");

            await _ctx.EnableAsync(EnableCap.DEPTH_TEST);
            {
                var go = new GameObject();
                go.Renderable.Mesh = Meshloader.Cube;
                go.Transform.Scale = new vec3(1, 1, 1);
                go.Transform.Axis = new vec3(0, 0, 1);
                await AddGameObject(go);
            }
            {
                var rand = new Random();
                for (var i = 0; i < 10; i++)
                {
                    var go = new GameObject();
                    go.Renderable.Mesh = Meshloader.Cube;
                    go.Transform.Scale = new vec3((rand.Next(150) + 50) / 100f);
                    go.Transform.Axis = new vec3(0.5f, 0.5f, 1);
                    go.Transform.Angle = (float) (rand.NextDouble() * Math.PI * 2);
                    go.Transform.Position = new vec3((float) (rand.NextDouble() * 20 - 10), (float) (rand.NextDouble() * 20 - 10), (float) (rand.NextDouble() * 20 - 10));
                    await AddGameObject(go);
                }
            }
        }

        private float CreateColor(float time)
        {
            return MathF.Cos(time) / 2 + 0.5f;
        }

        public async ValueTask AddGameObject(GameObject go)
        {
            await go.Renderable.Mesh.CreateBuffers(_ctx);
            _gameObjects.Add(go);
        }
    }
}