﻿using GlmNet;

namespace BlazorWASMTest.Code
{
    public class GameObject
    {
        public Transform Transform;
        public Renderable Renderable;
    }

    public struct Renderable
    {
        public Mesh Mesh;
    }

    public struct Transform
    {
        public vec3 Position;
        public vec3 Scale;
        public vec3 Axis;
        public float Angle;

        public mat4 GetMatrix()
        {
            var mat = mat4.identity();
            mat = glm.scale(mat, Scale);
            mat = glm.rotate(mat, Angle, Axis);
            mat = glm.translate(mat, Position);
            return mat;
        }
    }
}