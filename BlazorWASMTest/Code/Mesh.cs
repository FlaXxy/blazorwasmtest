﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Blazor.Extensions.Canvas.WebGL;
using GlmNet;

namespace BlazorWASMTest.Code
{
    public class Mesh
    {
        private readonly ushort[] _indices;
        private readonly VertexData[] _vertices;
        public readonly Primitive Primitive;
        private WebGLBuffer _indexBuffer;
        private WebGLBuffer _vertexBuffer;

        public Mesh(ushort[] indices, VertexData[] vertices, Primitive primitive = Primitive.TRIANGLES)
        {
            _indices = indices;
            _vertices = vertices;
            Primitive = primitive;
        }

        public int IndexCount => _indices.Length;

        internal async ValueTask CreateBuffers(WebGLContext ctx)
        {
            if (_vertexBuffer != null || _indexBuffer != null)
                return;
            var vertices = new List<float>(4 * 8 * _vertices.Length);
            foreach (var vertexData in _vertices)
            {
                vertices.Add(vertexData.Position.x);
                vertices.Add(vertexData.Position.y);
                vertices.Add(vertexData.Position.z);
                vertices.Add(vertexData.UVX);
                vertices.Add(vertexData.Normal.x);
                vertices.Add(vertexData.Normal.y);
                vertices.Add(vertexData.Normal.z);
                vertices.Add(vertexData.UVY);
            }


            _vertexBuffer = await ctx.CreateBufferAsync();
            await ctx.BindBufferAsync(BufferType.ARRAY_BUFFER, _vertexBuffer);
            await ctx.BufferDataAsync(BufferType.ARRAY_BUFFER, vertices.ToArray(), BufferUsageHint.STATIC_DRAW);

            _indexBuffer = await ctx.CreateBufferAsync();
            await ctx.BindBufferAsync(BufferType.ELEMENT_ARRAY_BUFFER, _indexBuffer);
            await ctx.BufferDataAsync(BufferType.ELEMENT_ARRAY_BUFFER, _indices, BufferUsageHint.STATIC_DRAW);
        }

        public async ValueTask BindMesh(WebGLContext ctx)
        {
            await ctx.BindBufferAsync(BufferType.ARRAY_BUFFER, _vertexBuffer);
            await ctx.BindBufferAsync(BufferType.ELEMENT_ARRAY_BUFFER, _indexBuffer);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct VertexData
    {
        public vec3 Position;
        public float UVX;
        public vec3 Normal;
        public float UVY;
    }
}